package com.example.incivisme;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.incivisme.pojo.Incidencia;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.annotations.Nullable;

import java.util.Arrays;
import java.util.Objects;

public class MapaFragment extends Fragment{

    private static final int LOCATION_REQUEST_CODE = 1;
    private static SupportMapFragment mapFragment;
    private GoogleMap mMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mapa_inciv, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase
                .getInstance("https://incivisme-cd209-default-rtdb.europe-west1.firebasedatabase.app/")
                .getReference();

        DatabaseReference users = base.child("users");
        String uidString = auth.getUid();
        if (uidString == null){
            Log.d("UID", "onCreateView: NULL UID STRING");
        }else {
            DatabaseReference uid = users.child(uidString);
            DatabaseReference incidencies = uid.child("incidencies");

            SharedViewModelNotif model = ViewModelProviders.of(requireActivity()).get(SharedViewModelNotif.class);

            mapFragment.getMapAsync(map -> {
                if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    map.setMyLocationEnabled(true);

                    MutableLiveData<LatLng> currentLatLng = model.getCurrentLatLng();
                    LifecycleOwner owner = getViewLifecycleOwner();
                    currentLatLng.observe(owner, latLng -> {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                        map.animateCamera(cameraUpdate);
                        currentLatLng.removeObservers(owner);
                    });

                    incidencies.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            Incidencia incidencia = dataSnapshot.getValue(Incidencia.class);

                            Log.e("INCIDENCIA", incidencia.toString());

                            LatLng aux = new LatLng(
                                    Double.parseDouble(incidencia.getLatitud()),
                                    Double.parseDouble(incidencia.getLongitud())
                            );

                            IncidenciesInfoWindowAdapter customInfoWindow = new IncidenciesInfoWindowAdapter(
                                    getActivity()
                            );

                            Marker marker = map.addMarker(new MarkerOptions()
                                    .title(incidencia.getProblema())
                                    .snippet(incidencia.getDireccio())
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                    .position(aux));
                            marker.setTag(incidencia);
                            map.setInfoWindowAdapter(customInfoWindow);
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }else {
                    Log.d(TAG, "onCreateView: PROBLEM WITH PERMISSIONS");
                }

                try {
                    boolean success = map.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    requireActivity(), R.raw.map_style));

                    if (!success) {
                        Log.e(null, "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e(null, "Can't find style. Error: ", e);
                }

            });
        }

        return view;
    }

}