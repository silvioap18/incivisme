package com.example.incivisme;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.incivisme.databinding.FragmentLlistatIncivBinding;
import com.example.incivisme.pojo.Incidencia;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LlistatFragment extends Fragment {

    private String user;
    private FirebaseListAdapter<Incidencia> adapter;
    private DatabaseReference users;

    @Override
    public void onStart() {
        super.onStart();
        listData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_sign_out, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refreshButton) {

        }
        return super.onOptionsItemSelected(item);
    }

    public void listData() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance("https://incivisme-cd209-default-rtdb.europe-west1.firebasedatabase.app/")
                .getReference();
        users = base.child("users");

        user = auth.getUid();
        if (user != null) {
            Log.d("USER llistat", user);
            DatabaseReference uid = users.child(user);

            DatabaseReference incidencies = uid.child("incidencies");


            FirebaseListOptions<Incidencia> options = new FirebaseListOptions.Builder<Incidencia>()
                    .setQuery(incidencies, Incidencia.class)
                    .setLayout(R.layout.lv_row_detail_incivisme)
                    .setLifecycleOwner(this)
                    .build();


            adapter = new FirebaseListAdapter<Incidencia>(options) {
                @Override
                protected void populateView(View v, Incidencia model, int position) {
                    TextView lvRowNum = v.findViewById(R.id.lvNumRow);
                    TextView txtDescripcio = v.findViewById(R.id.txtDescripcio);
                    TextView txtAdreca = v.findViewById(R.id.txtAdreca);
                    int rowNumber = position + 1;
                    lvRowNum.setText(String.valueOf(rowNumber));
                    txtDescripcio.setText(model.getProblema());
                    txtAdreca.setText(model.getDireccio());

                    Log.d("OUTPUT INCIDENCIA", position + " - " + model.getProblema() + " - " + model.getDireccio());

                }
            };
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentLlistatIncivBinding binding = FragmentLlistatIncivBinding.inflate(inflater);

        View view = binding.getRoot();

        listData();

        if (user != null) {
            binding.lvIncidencies.setAdapter(adapter);
        }

        return view;
    }
}