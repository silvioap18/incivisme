package com.example.incivisme;


import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.annotation.GlideModule;
import com.example.incivisme.databinding.FragmentNotificarIncivBinding;
import com.example.incivisme.pojo.Incidencia;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;


public class NotificarFragment extends Fragment {

    private SharedViewModelNotif model;

    String mCurrentPhotoPath;
    private Uri photoURI;
    static final int REQUEST_TAKE_PHOTO = 1;

    FirebaseStorage storage;
    StorageReference storageRef;
    String downloadUrl;

    FragmentNotificarIncivBinding binding;

    public NotificarFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_sign_out, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.signOutButton){
            AuthUI.getInstance()
                    .signOut(requireContext())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            FirebaseAuth auth = FirebaseAuth.getInstance();
                            if (auth.getCurrentUser() == null) {
                                // Choose authentication providers
                                startActivityForResult(
                                        AuthUI.getInstance()
                                                .createSignInIntentBuilder()
                                                /*.setLogo()*/
                                                .setAvailableProviders(
                                                        Arrays.asList(
                                                                new AuthUI.IdpConfig.EmailBuilder()
                                                                        /*.enableEmailLinkSignIn()
                                                                        .setAllowNewAccounts(true)*/
                                                                        .build(),
                                                                new AuthUI.IdpConfig.GoogleBuilder()
                                                                        /*.setSignInOptions(gso)*/
                                                                        .build()
                                                        )
                                                )
                                                .build(),
                                        0);
                            }
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentNotificarIncivBinding.inflate(inflater);

        View view = binding.getRoot();


        model = ViewModelProviders.of(getActivity()).get(SharedViewModelNotif.class);


        model.getCurrentAddress().observe(getViewLifecycleOwner(), address -> {
            binding.txtDireccio.setText(getString(R.string.address_text,
                    address, System.currentTimeMillis()));
        });
        model.getCurrentLatLng().observe(getViewLifecycleOwner(), latlng -> {
            binding.txtLatitud.setText(String.valueOf(latlng.latitude));
            binding.txtLongitud.setText(String.valueOf(latlng.longitude));
        });


        model.getProgressBar().observe(getViewLifecycleOwner(), visible -> {
            if (visible)
                binding.loading.setVisibility(ProgressBar.VISIBLE);
            else
                binding.loading.setVisibility(ProgressBar.INVISIBLE);
        });

        model.switchTrackingLocation();

        binding.buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();

        });

        binding.buttonNotificar.setOnClickListener(button -> {

            storage = FirebaseStorage.getInstance("gs://incivisme-cd209.appspot.com");
            storageRef = storage.getReference();

            Log.d(TAG, "onCreateView: HASTA AQUI LLEGA");

            if (mCurrentPhotoPath != null){
                StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
                UploadTask uploadTask = imageRef.putFile(photoURI);

                uploadTask.addOnSuccessListener(taskSnapshot -> {
                    Log.d("METADATA",Objects.requireNonNull(taskSnapshot.getMetadata()).toString());
                    imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                        Uri downloadUri = task.getResult();
                        Glide.with(this).load(downloadUri).into(binding.foto);
                        downloadUrl = downloadUri.toString();

                        Incidencia incidencia = new Incidencia();
                        incidencia.setDireccio(binding.txtDireccio.getText().toString());
                        incidencia.setLatitud(binding.txtLatitud.getText().toString());
                        incidencia.setLongitud(binding.txtLongitud.getText().toString());
                        incidencia.setProblema(binding.txtDescripcio.getText().toString());
                        incidencia.setUrl(downloadUrl);

                        FirebaseAuth auth = FirebaseAuth.getInstance();
                        DatabaseReference base = FirebaseDatabase
                                .getInstance("https://incivisme-cd209-default-rtdb.europe-west1.firebasedatabase.app/")
                                .getReference();

                        DatabaseReference users = base.child("users");
                        String uidString = auth.getUid();
                        DatabaseReference uid = users.child(Objects.requireNonNull(uidString));
                        DatabaseReference incidencies = uid.child("incidencies");

                        DatabaseReference reference = incidencies.push();
                        reference.setValue(incidencia);

                        Toast.makeText(getContext(), "Avís donat", Toast.LENGTH_SHORT).show();


                    });

                });
            }else {
                Toast.makeText(getContext(),
                        "Es necessari una foto!", Toast.LENGTH_SHORT).show();
            }



        });

        return view;
    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                requireContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.e("ERROR FOTO", e.getMessage());
                e.printStackTrace();
            }


            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(requireContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(photoURI).into(binding.foto);
            } else {
                Toast.makeText(getContext(),
                        "La foto no s'ha fet!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}