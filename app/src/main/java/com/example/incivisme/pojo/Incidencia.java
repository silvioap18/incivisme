package com.example.incivisme.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Incidencia {

    private String latitud;
    private String longitud;
    private String direccio;
    private String problema;
    private String url;

}
